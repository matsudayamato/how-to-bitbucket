# how to bitbucket
##　BITBUCKETを使う！
- gitのインストール
    - 'gitの確認'
        - git --version
    -　'gitのインストール'
        - sudo apt update
        - sudo apt-get install git
    - 'bitbucketのユーザー名、メールアドレスの指定'
        - cd
        - git config --global user.name "登録したユーザー名"
        - git config --global user.email "登録したメールアドレス"
    - 'ユーザー名、メールアドレスの確認'
        - git config --list
    - '†注意事項†'
        - ユーザー名、メールアドレスの指定はホームでやったほうがいい
        - ユーザー名、メールアドレスの指定のとき、nameと"登録したユーザー名"の間のスペース、emailと"登録したメールアドレス"の間のスペースを忘れないこと
-  bitbucketのレポジトリを使おう
    - レポジトリの作成
        - bitbucketのアカウントを作ったら、まずは好きな名前でレポジトリを作成する
        - bitbucketのレポジトリのページができたらページ右上にあるクローンの作成から、httpsをコピーする
    - レポジトリをlocalへコピー
        - 端末上でさっきコピーしたhttpsの方のコマンドを実行　↓↓↓↓
        - git clone https://ユーザー名@bitbucket.org/ユーザー名/レポジトリ名.git
    - localでファイルをつくる
        - 自分が作ったレポジトリに移動　↓↓↓↓
        - cd レポジトリ名
        - 何かファイルをつくって、保存する
        - git status 　で状態を確認する
    - push
        - レポジトリ内でする作業が終わったら、localで変更した内容をbitbucket上にpushしよう
        - 以下のコマンドを順に実行
        - git add .
        - git commit -m "変更した内容がわかるようなメッセージ"
        - git push origin master (passwordを求められることがある)

##　BRANCHを使ってみる
- branchを作って、masterとは別の場所で作業しよう
	- branch
		- 端末で以下のコマンドを実行しよう
		- cd　レポジトリ名
		- git checkout -b ブランチ名
		- git branch 　で現在いるbranchを確認
	- branch での作業後
		- git add .
		- git commit -m "メッセージ"
		- git push origin ブランチ名
